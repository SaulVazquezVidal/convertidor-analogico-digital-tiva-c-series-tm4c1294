#include <stdint.h>
#include <stdbool.h>
#include "driverlib/sysctl.h"
#include "inc/tm4c1294ncpdt.h"

#define ValPLLFREQ0 0X00800060;
#define ValPLLFREQ1 0X00000004;

void delayMs(uint32_t ui32Ms);
void complementa(void);

uint32_t  ValorADC, ValorTemp, retardo;
float ValorADCf, ret;
uint32_t Semiperiodo_delay;

void delayMs(uint32_t ui32Ms) {
    // 1 clock cycle = 1 / SysCtlClockGet() second
    // 1 SysCtlDelay = 3 clock cycle = 3 / SysCtlClockGet() second
    // 1 second = SysCtlClockGet() / 3
    // 0.001 second = 1 ms = SysCtlClockGet() / 3 / 1000
    SysCtlDelay(ui32Ms * (16000000 / 3 / 1000));
}

void complementa(void){
    GPIO_PORTN_DATA_R ^= 0x02;
}

int main(void){
    //----------------------------------------------------------------------------------------------------------------------
    //---------------------------------------- INICIALIZAMOS EL PLL --------------------------------------------------------
    //SE INICIALIZA EL PLL PARA CONFIGURAR EL ADC.
    SYSCTL_PLLFREQ1_R = ValPLLFREQ1;
    SYSCTL_PLLFREQ0_R = ValPLLFREQ0;
    while((SYSCTL_PLLSTAT_R&0x01)==0);
    //----------------------------------------------------------------------------------------------------------------------
    //----------------------------------------------------------------------------------------------------------------------


    //----------------------------------------------------------------------------------------------------------------------
    //------------------------------------- HABILITAMOS PUERTOS Y MODULO ADC -----------------------------------------------
    //habilitar reloj para los puertos utilizados
    SYSCTL_RCGCGPIO_R  |= SYSCTL_RCGCGPIO_R9 |SYSCTL_RCGCGPIO_R12;   //RELOJ PARA PUERTO K Y PUERTO N
    SYSCTL_RCGCADC_R  |= SYSCTL_RCGCADC_R0;   //SE HABILITA EL RELOJ PARA EL ADC (SE HABILITA EL REGISTRO 0 del RCGCADC)
    ValorADC=65565; //tiempo para que el reloj llegue a los modulos (Creo que esto es un delay para que los modulos se habiliten)
    //----------------------------------------------------------------------------------------------------------------------
    //----------------------------------------------------------------------------------------------------------------------


    //----------------------------------------------------------------------------------------------------------------------
    //------------------------------------- CONFIGURAMOS LOS PUERTOS -------------------------------------------------------
    //SE CONFIGURA PK0 PARA USAR AIN16 Y SE CONFIGURA EL LED2 QUE CORRESPONDE A PN1
    GPIO_PORTK_AMSEL_R |=0X01;  //HABILITA MODULO ANALOGICO DEL REGISTRO 0 (PK0 PARA AIN16)
    GPIO_PORTK_DEN_R &= ~0X01;  //DESHABILITA EL BUFFER DIGITAL

    GPIO_PORTN_DATA_R = 0x00;    //SE PREPARA EL PUERTO N
    GPIO_PORTN_DEN_R = 0X02;    //SE HABILITA EL REGISTRO 0
    GPIO_PORTN_DIR_R = 0X02;    //SE CONFIGURA COMO SALIDA
    //----------------------------------------------------------------------------------------------------------------------
    //----------------------------------------------------------------------------------------------------------------------

    //----------------------------------------------------------------------------------------------------------------------
    //---------------------------------------- CONFIGURAMOS EL ADC ---------------------------------------------------------
    //SE DEFINE LA CONFIGURACION DE LA PRIORIDAD
    ADC0_SSPRI_R=0X00003210; //se queda con las mismas prioridades (0 mayor prioridad y 3 menor prioridad)
    //ADC0_CC_R=0X170; //RELOJ DEL ADC EL VCO del PLL /24

    //CONFIGURACION DE TASA DE MUESTREO. PAG.1159
    ADC0_PC_R=0x07;//1Ms/S

    //SE DESHABILITA EL SECUENCIADOR. VAMOS A UTILIZAR EL SECUENCIADOR 0
    ADC0_ACTSS_R  = 0;  // DESHABILITA EL SECUENCIADOR DURANTE LA CONFIGURACION DEL ADC.

    //SE DEFINE EL MODO DE DISPARO: SOFTWARE, TIMER, PWM, COMPARADOR, ALWAYS-ON (Pag. 1091)
    ADC0_EMUX_R   = 0X0000;   //inicio de la conversion por software con bit SSn en el ADCPSSI (en ADCPSSI se indica el dispado por software)


    //SELECCION DE CANAL (AQUI SELECCIONAMOS EL CANAL 16, YA QUE UTILIZAMOS AIN16, UBICADO EN PK0)
    ADC0_SSEMUX0_R  = 0x0011;  //obtener entrada del primer grupo de 16 canales (0 al 15) (Este es el 5o bit (MSB) faltante del registro)[Pag. 1146]
    ADC0_SSMUX0_R  = 0x0000;   //obtener entrada del canal 0 (Los 4 bits (LSB) del registro para seleccionar el canal) [Pag. 1141]


    //**LE DECIMOS AL SECUENCIADOR LO QUE VA A HACER** [Ts=1 Ie=0 END=1 D=0] [Ts=0 Ie=0 END=0 D=0] -> 0xA0 = 0b1010.0000
    ADC0_SSCTL0_R   |= 0xE0;    //SE PONE LA BANDERA PARA QUE TERMINE LA PRIMERA MUESTRA

    ADC0_SSTSH0_R = 0x0060;

    //HA TERMINADO LA CONFIGURACION. SE HABILITA EL SECUENCIADOR
    ADC0_ACTSS_R |= 0x01;        //HABILITA EL SECUENCIADOR 0 DEL ADC0. PAG. 1077.


    //----------------------------------------------------------------------------------------------------------------------
    //------------------------------------------ DESHABILITA EL PLL --------------------------------------------------------
    //Como ya terminamos de configurar el ADC, ya podemos deshabilitar el PLL, ya que solo se requiere encendido en la configuracion.
    SYSCTL_PLLFREQ0_R=0; //DESHABILITA EL PLL
    SYSCTL_PLLFREQ1_R=0;
    //EL PLL YA ESTA DESHABILITADO.
    //----------------------------------------------------------------------------------------------------------------------
    //----------------------------------------------------------------------------------------------------------------------

    while(1){
        //SE REALIZA DISPARO POR SOFTWARE. EMPEZAR SECUENCIA DE CONVERSION (SE HACE DISPARO POR SOFTWARE). PAG. 1103
        ADC0_PSSI_R |= 0x0001;             //ESTE BIT SE REGRESA AUTOMATICAMENTE A 0 POR SOFTWARE.

        while((ADC0_RIS_R & 0x0001 == 0));   //ESPERA A QUE TERMINE LA CONVERSION (LA BANDERA DE ADC_RIS INDICA QUE LA CONVERSION TERMINO)
        ValorADC  = ADC0_SSFIFO0_R ;    //LEER EL RESULTADO DE LA PRIMERA CONVERSION
        ValorTemp  = ADC0_SSFIFO0_R ;   //LEER EL RESULTADO DE LA SEGUNDA CONVERSION

        ValorADCf = 147.5 - ((75*3.3*(float)ValorTemp) / (4096)); //convertir a grados
        //ret = (0.000234375*ValorADC) + 0.04;
        //retardo = (SysCtlClockGet()/3)*ret;
        //SysCtlDelay(retardo);
        Semiperiodo_delay = (((500.0 - 20.0)/(4095.0))*ValorADC) + 20.0;//EN MILISEGUNDOS
        delayMs(Semiperiodo_delay);
        complementa();


        ADC0_ISC_R = 0x0001;
    }
}
