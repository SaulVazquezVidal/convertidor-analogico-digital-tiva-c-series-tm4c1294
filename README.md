# Convertidor Analogico Digital - TIVA C Series TM4C1294

Desarrollar un programa que lea el sensor de temperatura y que, por medio de una señal analógica
externa, regule la velocidad de parpadeo de un LED; compilarlo, cargarlo en la tarjeta de desarrollo,
alambrar el hardware y verificar su funcionamiento.

## Acknowledgements

 - [Awesome Readme Templates](https://awesomeopensource.com/project/elangosundar/awesome-README-templates)
 - [Awesome README](https://github.com/matiassingers/awesome-readme)
 - [How to write a Good readme](https://bulldogjob.com/news/449-how-to-write-a-good-readme-for-your-github-project)




## Appendix

 - La entrada analógica externa será por AIN16 y, como se muestra en la figura 2, se simulará
mediante un potenciómetro mayor o igual a 5k (opción A) o un arreglo de resistencias
(opción B).
 - El LED2 deberá parpadear con una variación del periodo (aproximado) de 40 ms para
cunado la entrada AIN16 = 0 V hasta 1s para cuando AIN16 = 3.3V. Considere la siguiente
ayuda de acuerdo con la figura 1 y las ecuaciones (1) y (2):

$retardo = (X + Y * ADC\_RES) $

$SysCtlDelay(retardo)$



![Diagrama Esquematico](https://lh4.googleusercontent.com/f8JdDlIeSDxLCQon-f52YOik3CJXindeB9guWjuball2tu3aJvGQmhDxVcJGnfiqD-s=w2400)


## Authors

- [@SaulVazquezVidal](https://gitlab.com/SaulVazquezVidal)



## Badges



![MIT License](https://img.shields.io/apm/l/atomic-design-ui.svg?)







## Demo

![Diagrama Esquematico](https://lh6.googleusercontent.com/XxCwT1jCRVgjkexFMKGZwJmly-h3NK6cmv2yZbuROrpvS528YGld8co2IXhY6vUdMeg=w2400)



## Support

For support, email saul.vazquez.vida@gmail.com



## Screenshots
Y a continuación se muestra una captura de pantalla con los resultados obtenidos en ejecución.

![App Screenshot](https://lh3.googleusercontent.com/RLVh5qXVAXTBxMf0e8na-Tp7hU0ndLbB1w116ROrubh7AB6O7i_0TpYXth2JEVw-sqY=w2400)

